*** Settings ***
Documentation    Un premier test d’accès à la page de login.
Library    SeleniumLibrary

*** Variables ***
${url}    https://katalon-demo-cura.herokuapp.com/
${browser}    firefox
${check}    Yes

*** Keywords ***
Open Browser To Login Page
    # implicit wait #
    Set Selenium Implicit Wait    2 seconds
    # Ouvrir le navigateur
    Open Browser    ${url}     ${browser}
    # Agrandir la fenêtre
    Maximize Browser Window
    # Cliquer sur le bouton "Make Appointment"
    Click Element    id:btn-make-appointment
    Wait Until Page Contains Element    xpath://input[@id='txt-username']
    
Login To Book Appointment
    [Arguments]    ${login}    ${password}
    # Remplir le formulaire de connexion
    Input Text    xpath://input[@id='txt-username']    ${login}
    Input Text    xpath://input[@id='txt-password']    ${password}
    # Valider le formulaire
    Click Element    xpath://button[@id='btn-login']
    Wait Until Page Contains Element    xpath://section[@id='appointment']  

Book Appointment
    [Arguments]    ${Facility}    ${Checkbox}    ${Program}    ${Date}    ${Comment}
    # Remplir le formulaire
    Select From List By Value    xpath://*[@id="combo_facility"]    ${Facility}
    Select Checkbox    xpath://*[@id="chk_hospotal_readmission"]
    Select Radio Button    programs    ${Program}
    Input Text    xpath://*[@id="txt_visit_date"]    ${Date}
    Input Text    xpath://*[@id="txt_comment"]    ${Comment}
    Click Element    xpath://*[@id="btn-book-appointment"]
    Wait Until Page Contains Element    xpath://*[@id="summary"]/div/div/div[1]/h2 
    Element Text Should Be    xpath://*[@id="facility"]    ${Facility}
    Element Text Should Be    xpath://*[@id="hospital_readmission"]   ${Checkbox}
    Element Text Should Be    xpath://*[@id="program"]    ${Program}
    Element Text Should Be    xpath://*[@id="visit_date"]    ${Date}
    Element Text Should Be    xpath://*[@id="comment"]    ${Comment}
    Click Element    xpath://*[@id="summary"]/div/div/div[7]/p/a
    
End Of Test And Close Browser
    Close Browser

*** Test Cases ***
Login page test success
    Open Browser To Login Page
    Login To Book Appointment    John Doe    ThisIsNotAPassword
    End Of Test And Close Browser
Login page test failure
    Open Browser To Login Page
    Login To Book Appointment    John Doe2    ThisIsNotAPassword
    End Of Test And Close Browser
Book Appointment Test
    Open Browser To Login Page
    Login To Book Appointment    John Doe    ThisIsNotAPassword
    Book Appointment    Hongkong CURA Healthcare Center    Yes    Medicaid    02/07/2022    un commentaire
    End Of Test And Close Browser