*** Settings ***
Resource	squash_resources2.resource
*** Variables ***
${url}    https://katalon-demo-cura.herokuapp.com/
${browser}    firefox
&{Jdd1}    facility=Tokyo    healthcareProgram=radio_program_medicare    verifHeathcareProgram=Medicare
&{Jdd2}    facility=Hongkong    healthcareProgram=radio_program_medicaid    verifHeathcareProgram=Medicaid
&{Jdd3}    facility=Seoul    healthcareProgram=radio_program_none    verifHeathcareProgram=None

*** Keywords ***
Test Setup
	Open Browser    ${url}    ${browser}
    Maximize Browser Window


Test Teardown
	Sleep    5
    Close Browser

*** Test Cases ***
Test 04_1
    [Tags]    run-once
	[Setup]	Test Setup

	Given Je suis connecté sur la page de rendez-vous
	When Je renseigne les informations obligatoires    ${Jdd1.facility}
	And Je choisis HealthcareProgram    ${Jdd1.healthcareProgram}
	And Je clique sur Book Appointment
	Then Le rendez-vous est confirmé et le HealthcareProgram est affiché    ${Jdd1.verifHeathcareProgram}    ${Jdd1.facility}

	[Teardown]	Test Teardown

Test 04_2
	[Setup]	Test Setup

	Given Je suis connecté sur la page de rendez-vous
	When Je renseigne les informations obligatoires    ${Jdd2.facility}
	And Je choisis HealthcareProgram    ${Jdd2.healthcareProgram}
	And Je clique sur Book Appointment
	Then Le rendez-vous est confirmé et le HealthcareProgram est affiché    ${Jdd2.verifHeathcareProgram}    ${Jdd2.facility}

	[Teardown]	Test Teardown

Test 04_3
	[Setup]	Test Setup

	Given Je suis connecté sur la page de rendez-vous
	When Je renseigne les informations obligatoires    ${Jdd3.facility}
	And Je choisis HealthcareProgram    ${Jdd3.healthcareProgram}
	And Je clique sur Book Appointment
	Then Le rendez-vous est confirmé et le HealthcareProgram est affiché    ${Jdd3.verifHeathcareProgram}    ${Jdd3.facility}

	[Teardown]	Test Teardown